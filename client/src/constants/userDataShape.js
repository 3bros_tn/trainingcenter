import PropTypes from 'prop-types';

const userDataShape = {
  id: PropTypes.any.isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  phoneNumber: PropTypes.string.isRequired,
  birthdate: PropTypes.string.isRequired,

  role: PropTypes.string.isRequired,

  // if role is formateur
  typeformateur:PropTypes.string,
  formations:PropTypes.string,



  // if role is stagiaire
  company: PropTypes.string,
  formation:PropTypes.string,

  avatar: PropTypes.string,
};

export default userDataShape;
