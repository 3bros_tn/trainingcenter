import Api from '../api/config';

const API_PREFIX = 'course';
const API_CATEGORY_PREFIX = 'category';
//const category="JAVA"

const getAllCourses = () => Api().get(`${API_PREFIX}/all`);
const getCourseByCategory = category => Api().get(`${API_PREFIX}/test/${category}`);

const getAllCategories = () => Api().get(`${API_CATEGORY_PREFIX}/all`);



const getCourseById = courseId => Api().get(`${API_PREFIX}/${courseId}`);

const enrollCourse = courseId =>
  Api().post(`${API_PREFIX}/${courseId}/subscribe`);
const leaveCourse = courseId =>
  Api().post(`${API_PREFIX}/${courseId}/unsubscribe`);

const createCourse = courseData =>
  Api().post(`${API_PREFIX}/create`, courseData);
const updateCourse = (courseId, courseData) =>
  Api().put(`${API_PREFIX}/${courseId}/update`, courseData);

const getLecturerCourses = () => Api().get(`lecturer/my_courses`);
const getListenerCourses = () => Api().get(`listener/my_courses`);
const getAllListeners = () => Api().get(`listener/all`);

export const coursesService = {
  getCourseByCategory,
  getAllCourses,
  getAllCategories,
  getCourseById,
  enrollCourse,
  leaveCourse,
  createCourse,
  updateCourse,
  getLecturerCourses,
  getListenerCourses,
  getAllListeners
};
