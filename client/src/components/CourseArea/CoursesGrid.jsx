import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { getAllCategories } from '../../_actions/courses.actions';
import { connect } from 'react-redux';
import { Link as RouterLink, useRouteMatch } from 'react-router-dom';
import { CircularProgress, ButtonGroup, Button, Grid } from '@material-ui/core';
import CourseCard from './CourseCard';
import { useStyles } from './styles';
const AddButtonStyle = {
  marginBottom:10,
  marginLeft: '80%',
};
const CoursesGrid = ({ items, isLoading,getAllCategories, categories }) => {
  const [courses, setCourses] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    setCourses(items);
    
  }, [items,getAllCategories]);

  useEffect(() => {
    getAllCategories();
  }, [getAllCategories]);

  return isLoading ? (
    <CircularProgress size={20} />
  ) : (
    <>
      <div className={classes.filterContainer}>
        <ButtonGroup
          variant="outlined"
          color="secondary"
          aria-label="select courses by category"
        >
          <Button id="all_courses" onClick={() => setCourses(items)}>
            All
          </Button>
          {categories.map(({ id, categoryName }) => (

            <Button
              key={id}
              id={id}
              onClick={() =>
                setCourses(items.filter(item => item.category === categoryName))
              }
            >
              {categoryName}
            </Button>
            
          ))}
        </ButtonGroup>
        <Button  style={AddButtonStyle} component={RouterLink} to={`/course/create`}>
          {'Ajouter'}
        </Button>
      </div>
      {courses.map(itemData => (
        <Grid
          item
          xs={12}
          sm={6}
          lg={4}
          key={itemData.id + itemData.courseName}
        >
          <CourseCard courseData={itemData} />
        </Grid>
      ))}
    </>
  );
};

CoursesGrid.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      courseName: PropTypes.string.isRequired,
      courseDescription: PropTypes.string.isRequired,
      courseDuration: PropTypes.number.isRequired,
      startDate: PropTypes.any.isRequired,
      category: PropTypes.string.isRequired,
      imageUrl: PropTypes.string.isRequired,
    }),
  ).isRequired,
  isLoading: PropTypes.bool,
  //categories: PropTypes.array.isRequired,
 // getAllCategories: PropTypes.func.isRequired,
};

CoursesGrid.defaultProps = {
  isLoading: false,
};
const mapDispatchToProps = {
  getAllCategories
};
const mapStateToProps = state => ({
  categories: state.coursesReducer.categories ,
  categoriesLoading: state.coursesReducer.isGettingCategorieProcessing
});
//export default CoursesGrid;
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CoursesGrid);