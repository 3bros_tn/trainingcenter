import MenuIcon from '@mui/icons-material/Menu';
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Toolbar from '@mui/material/Toolbar';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';
import * as React from 'react';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import {CategoryDialogContent, CustomExample } from '../../Pages/Admin/CategoryDialogContent'
import { getAllUsers, getCurrentUser, userLogout } from '../../_actions';
import { ModalContent, ModalFooter, ModalButton, useDialog,CustomDialog } from 'react-st-modal';
import CategoryDialogBox from '../CategoryDialogBox';
import { Link as RouterLink, useRouteMatch } from 'react-router-dom';
import { history } from '../../_utils/history';
const pages = ['Users List', 'Add user', 'Categories', 'Training'];
const settings = [ 'Account', 'Logout'];
const menueUsers= [ 'Afficher', 'Lister'];
const ResponsiveAppBar = ({ getAllUsers, getCurrentUser, userLogout, user}) => {


  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const [selectedItem, setSelectedItem] = React.useState();
  const { path } = useRouteMatch();

  useEffect(() => {
    getCurrentUser();
  }, [getCurrentUser]);
  console.log("ROOOOOOLE form==>"+user.role);

  const handleOpenNavMenu = event => {
    setAnchorElNav(event.currentTarget);
    console.log(event.currentTarget + ' navmenuclicked');
  };
  const handleOpenUserMenu = event => {
    setAnchorElUser(event.currentTarget);
    console.log(event.currentTarget.value + ' usermenuclicked');
  };
  
  const handleOpenCoursesMenu = event => {
    setAnchorElNav(event.currentTarget);
    console.log(event.currentTarget.value + ' usermenuclicked');
  };

  const handleCloseNavMenu = () => {
    //  console.log(anchorElNav.key + " navmenuclickedtoclose");
    setAnchorElNav(null);
  };
  const handleSettingMenu = event => {
    console.log(user.role + " navmenuclickedtoclose");
    //setAnchorElNav(null);
    setAnchorElUser(null);
    switch (event.currentTarget.innerText) {
     /* case settings[0]:
        console.log(settings[0]);
        getAllUsers();
      // return 'bar';
      break;*/
      case settings[0]:
        console.log(settings[0]);
        history.push('/profile');
        /* if ((getCurrentUser().role === 'ROLE_ADMIN') || (getCurrentUser().role === 'ROLE_SUPER_ADMIN')) {
          history.push('/admin/users');
        } else {
          history.push('/profile');
        } */
        break;
      case settings[1]:
        console.log(settings[1]);
        userLogout();
        break;
       // props.userLogout;
      default:
        console.log(settings[0]);
    }
  };

  const handleCloseUserMenu = () => {
 //   console.log(anchorElUser + ' usermenuclickedtoclose');
    setAnchorElUser(null);
  };

  const handleClick = event => {
    console.log(event.currentTarget.innerText + ' usermenuclicked');
    //console.log(selectedItem)
    setAnchorElNav(null);
    switch (event.currentTarget.innerText) {
      case "category":
        console.log(event.currentTarget.innerText);
      
      // return 'bar';
      break;
      case "add user":
        console.log(pages[1]);
        break;
      case pages[2]:
        console.log(pages[2]);
      
        
        break;
      case pages[3]:
        console.log(pages[3]);
       
        break;
      default:
        console.log(pages[0]);
    }
  };
  if(JSON.parse(localStorage.getItem("role"))=="ROLE_ADMIN"
  || JSON.parse(localStorage.getItem("role"))=="ROLE_SUPER_ADMIN")
  {
  return (
  

    <AppBar position="static" style={{ background: '#4D8383' }}>
       <Container maxWidth="xl">
         <Toolbar disableGutters>
           <Typography
             variant="h6"
             noWrap
             component="div"
             sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
           >
             <img src={require('../../image/logo_lic_formation.png')} />
           </Typography>
           {/* <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
             <IconButton
               size="large"
               aria-label="account of current user"
               aria-controls="menu-appbar"
               aria-haspopup="true"
               onClick={handleOpenNavMenu}
               color="inherit"
             >
               <MenuIcon />
             </IconButton>
             
             <Menu
               id="menu-appbar"
               anchorEl={anchorElNav}
               anchorOrigin={{
                 vertical: 'bottom',
                 horizontal: 'left',
               }}
               keepMounted
               transformOrigin={{
                 vertical: 'top',
                 horizontal: 'left',
               }}
               open={Boolean(anchorElNav)}
               onClose={handleCloseNavMenu}
               sx={{
                 display: { xs: 'block', md: 'none' },
               }}
             >
               { {pages.map(page => (
                 <MenuItem key={page} value={page} onClick={handleC0lick}>
                   <Typography textAlign="center">{page}</Typography>
                 </MenuItem>
               ))} }
 
                <MenuItem key={"Category"} value={"category"}    onClick={handleClick , async () => {
             await CustomDialog(<CategoryDialogBox/>, {
             title: 'Create Category',
             showCloseIcon: true, });}} onClose={true} >
                   <Typography textAlign="center">{"category"}</Typography>
                    {/* <CategoryDialogBox></CategoryDialogBox>  }
                 </MenuItem>
 
 
                 <MenuItem key={"add user"}  onClick={handleClick}
                  component={RouterLink} to={`/admin/users/user/create`} >
                   <Typography textAlign="center">{"add user"}</Typography>
                 </MenuItem>
                 <MenuItem key={"courses"}  component={RouterLink} to={`/course/create`} >
                   <Typography textAlign="center">{"courses"}</Typography>
                 </MenuItem>
               
               { {pages.map((page , index) => (
                 <MenuItem key={index}  onClick={handleClick}  >
                   <Typography textAlign="center">{page}</Typography>
                 </MenuItem>
               ))} }
             </Menu>
               </Box>
         <Typography
             variant="h6"
             noWrap
             component="div"
             sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}
           >
             <img src={require('../../image/logo_lic_formation.png')} />
           </Typography>*/}
           <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            {/*  {pages.map(page => (
               <Button
                 key={page}
                 onClick={handleCloseNavMenu}
                 sx={{ my: 2, color: 'white', display: 'block' }}
               >
                 {page}
               </Button>
             ))} */}
                 <MenuItem key={"Category"} value={"category"}    onClick={handleClick , async () => {
             await CustomDialog(<CategoryDialogBox/>, {
             title: 'Create Category',
             showCloseIcon: true, });}} onClose={true} >
                   <Typography textAlign="center">{"Gestions des catégories"}</Typography>
                    {/* <CategoryDialogBox></CategoryDialogBox>  */}
                 </MenuItem> 
       
 
 
 
 
                <MenuItem key={"add user"}  onClick={handleClick}
                  component={RouterLink} to={`/admin/users/`} >
                   <Typography textAlign="center">{" Gestions users "}</Typography>
                 </MenuItem>
                 <MenuItem key={"courses"}  component={RouterLink} to={`/courses`} >
                   <Typography textAlign="center">{"Gestions des formations"}</Typography>
                            </MenuItem>
           </Box>
           <Box sx={{ flexGrow: 0 }}>
             <Tooltip title="Open settings">
               <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                 {/* <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" /> */}
                 <Avatar alt={user.name} src={`https://i.pravatar.cc/150?u={user.id}`}></Avatar>
               </IconButton>
             </Tooltip>
             <Menu
               sx={{ mt: '45px' }}
               id="menu-appbar"
               anchorEl={anchorElUser}
               anchorOrigin={{
                 vertical: 'top',
                 horizontal: 'right',
               }}
               keepMounted
               transformOrigin={{
                 vertical: 'top',
                 horizontal: 'right',
               }}
               open={Boolean(anchorElUser)}
               onClose={handleCloseUserMenu}
             >
               {settings.map(setting => (
                 // <MenuItem key={setting} onClick={userLogout}>
                 //   <Typography textAlign="center">{setting}</Typography>
                 // </MenuItem>
                 <MenuItem key={setting} onClick={handleSettingMenu}>
                   <Typography textAlign="center">{setting}</Typography>
                 </MenuItem>
 
                 //   <MenuItem key={setting} onClick={handleCloseUserMenu}>
                 //   <Typography textAlign="center">{setting}</Typography>
                 // </MenuItem>
               ))}
             </Menu>
           </Box>
 
 
 
          
         </Toolbar>
       </Container>
 
     </AppBar>
    
   );
} 
//else if(JSON.parse(localStorage.getItem("role"))=="ROLE_LECTURER")
else
{
  return (
  
<AppBar position="static" style={{ background: '#4D8383' }}>  
       <Container maxWidth="xl">
         <Toolbar disableGutters>
           <Typography
             variant="h6"
             noWrap
             component="div"
             sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
           >
             <img src={require('../../image/logo_lic_formation.png')} />
           </Typography>
           <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                <MenuItem key={"add user"}  onClick={handleClick}
                  component={RouterLink} to={`/calendar`} >
                   <Typography textAlign="center">{" Calendrier "}</Typography>
                 </MenuItem>
                 <MenuItem key={"courses"}  component={RouterLink} to={`/courses`} >
                   <Typography textAlign="center">{"Gestions des formations"}</Typography>
                            </MenuItem>
           </Box>
           <Box sx={{ flexGrow: 0 }}>
             <Tooltip title="Open settings">
               <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                 {/* <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" /> */}
                 <Avatar alt={user.name} src={`https://i.pravatar.cc/150?u={user.id}`}></Avatar>
               </IconButton>
             </Tooltip>
             <Menu
               sx={{ mt: '45px' }}
               id="menu-appbar"
               anchorEl={anchorElUser}
               anchorOrigin={{
                 vertical: 'top',
                 horizontal: 'right',
               }}
               keepMounted
               transformOrigin={{
                 vertical: 'top',
                 horizontal: 'right',
               }}
               open={Boolean(anchorElUser)}
               onClose={handleCloseUserMenu}
             >
               {settings.map(setting => (
                 <MenuItem key={setting} onClick={handleSettingMenu}>
                   <Typography textAlign="center">{setting}</Typography>
                 </MenuItem>

               ))}
             </Menu>
           </Box>  
         </Toolbar>
       </Container>
 
     </AppBar>
    
   );
}
};

ResponsiveAppBar.propTypes = {
  userLogout: PropTypes.func.isRequired,
  getCurrentUser: PropTypes.func.isRequired,
  getAllUsers: PropTypes.func.isRequired,
  CustomExample: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  currentUserId: state.userReducer.currentUserId,
  allUsers: state.userReducer.allUsers,
  user: state.userReducer.currentUser,
  CustomExample: state.userReducer.CustomExample,
});

const mapDispatchToProps = {
  getAllUsers,
  getCurrentUser,
  userLogout,
  CustomExample,
};
export default connect(mapStateToProps, mapDispatchToProps)(ResponsiveAppBar);
