import React from 'react';

import { ModalContent, ModalFooter,CustomDialog } from 'react-st-modal';
import { Button, Input} from '@material-ui/core';
import {NotificationContainer,NotificationManager} from 'react-notifications';
import Api from '../../api/config';
import { connect } from 'react-redux';

const CategoryDialogBox =() =>  {
 
  const [categoryName, setCategory] = React.useState("");
  class Category {
    constructor(name) {
      this.categoryName = name;
    }

  }

  const handleSubmit = (event) => {





     NotificationManager.success('catégorie ajouté avec succès', 'SUCCESS');
      Api().post('category/add_category', {categoryName});
    event.preventDefault();
  }

  return (
 <div>
       <form onSubmit={handleSubmit} >
      <ModalContent>
      <label>
       <p>Category :</p> 
        <Input
          name="categoryName"
          type="category"
          variant="outlined"
          fullWidth
          required
          position= "relative"
          value={categoryName}
          onChange={e => setCategory(e.target.value)}
          />
      </label>
      </ModalContent>
      <div>
      <ModalFooter >
      <Button type="submit" size="medium" variant="contained" color="secondary" flex="1" >
          {'Submit'}
        </Button>
        </ModalFooter>
      </div>
    </form>
    <NotificationContainer/>

  </div>
  );
}

export default CategoryDialogBox;
