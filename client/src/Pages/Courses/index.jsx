import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';
import { getAllCourses,getCoursesByCategory } from '../../_actions/courses.actions';
import DashboardArea from '../../components/DashboardArea';
import CoursesGrid from '../../components/CourseArea/CoursesGrid';
import ResponsiveAppBar from '../../components/ResponsiveAppBar';
import {
  Box,
  Typography,
  Button,
} from '@material-ui/core';
import { Link as RouterLink, useRouteMatch } from 'react-router-dom';
import CategoriesCoursesAutoComplete from '../../Pages/CourseCreator/CategoriesCoursesAutoComplete';

const Courses = ({ getAllCourses, courses, coursesLoading,getCoursesByCategory}) => {
  const { path } = useRouteMatch();

  useEffect(() => {
if(JSON.parse(localStorage.getItem("role"))=="ROLE_LECTURER")
    {
      //getCoursesByCategory(localStorage.getItem("categories"));
      //console.log("getCoursesByCategory",localStorage.getItem("categories"))
      getCoursesByCategory(JSON.parse(localStorage.getItem("categories")));
    }
    else 
    {
      getAllCourses();
    }
  }, [getAllCourses,getCoursesByCategory,]);
 

  //console.log("user",localStorage.getItem("role"))
  //console.log("get all courses 1 ",courses.map(itemData => (itemData.courseName)))
 // if(JSON.parse(localStorage.getItem("role"))=="ROLE_ADMIN"
//|| JSON.parse(localStorage.getItem("role"))=="ROLE_SUPER_ADMIN"
//)
   // {
      return (
       <div>

          <ResponsiveAppBar/>
          <Grid container spacing={0}>
          <Box py={1} px={2} my={1} display="flex" alignItems="baseline" justifyContent="space-between">
     
      </Box>
            <CoursesGrid items={courses} isLoading={coursesLoading} />
          </Grid>
       </div>
      
      );
  /*  }
    else
    {
      return (
        <DashboardArea>
          <Grid container spacing={3}>
            <CoursesGrid items={courses} isLoading={coursesLoading} />
          </Grid>
        </DashboardArea>
      );
    }*/
 
};

Courses.propTypes = {
  courses: PropTypes.array.isRequired,
  getAllCourses: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  courses: state.coursesReducer.courses,
  coursesLoading: state.coursesReducer.isGettingCourseProcessing,
});

const mapDispatchToProps = {
  getAllCourses,getCoursesByCategory
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Courses);
