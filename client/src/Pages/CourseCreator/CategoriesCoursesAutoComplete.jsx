import { getAllCategories } from '../../_actions/courses.actions';
import Autocomplete from '@mui/material/Autocomplete';
import { TextField } from '@material-ui/core';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fields } from '../../constants/courseFields';


const CategoriesCoursesAutoComplete = ({ getAllCategories, categories ,categoryChanger}) => {
    useEffect(() => {
      getAllCategories();
    }, [getAllCategories]);

    const category = []
    for (const [index, value] of categories.entries()) {
      category.push(value.categoryName)
    }

    return (
    <Autocomplete
        key={fields.CourseCategoryProps.id}
        {...fields.CourseCategoryProps}
        disablePortal
        id='category'
        name="category"
        options={category}
        onChange={ categoryChanger}
        getOptionLabel={(option)=>option}
        renderInput={(params)=>
            <TextField {...params}
            variant="outlined"
            fullWidth
            margin="normal"
            label="category"
            ></TextField>
        }
         ></Autocomplete>
         
         
      );


  }
  
  CategoriesCoursesAutoComplete.propTypes = {
    categories: PropTypes.array.isRequired,
    getAllCategories: PropTypes.func.isRequired,
  };
  const mapStateToProps = state => ({
    categories: state.coursesReducer.categories ,
    categoriesLoading: state.coursesReducer.isGettingCategorieProcessing
  });

  const mapDispatchToProps = {
    getAllCategories
  };
  export default connect(
    mapStateToProps,
    mapDispatchToProps,
  )(CategoriesCoursesAutoComplete);


  