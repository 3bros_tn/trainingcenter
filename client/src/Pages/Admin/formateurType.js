export const typeformateur = [
  {
    key: 'TYPE_INTERNE',
    name: 'Salarié',
  },
  {
    key: 'ROLE_EXTERNE',
    name: 'Externe',
  },
];
