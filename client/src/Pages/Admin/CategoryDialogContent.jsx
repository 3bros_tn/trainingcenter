import React from 'react';

import { ModalContent, ModalFooter, ModalButton, useDialog,CustomDialog } from 'react-st-modal';
import { Button, Input} from '@material-ui/core';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Api from '../../api/config';
import { InputOutlined, InputRounded } from '@material-ui/icons';

function CategoryDialogContent() {
 
  const [categoryName, setCategory] = React.useState("");
  class Category {
    constructor(name) {
      this.categoryName = name;
    }

  }

  const handleSubmit = (event) => {

    Api().post('category/add_category', {categoryName});
    NotificationManager.success('catégorie ajouté avec succès', 'SUCCESS');
    event.preventDefault();
  }

  return (
    <div>
    <form onSubmit={handleSubmit}>
      
      <ModalContent>
      <label>
       <p>Category:</p> 
        <Input
          name="categoryName"
          type="category"
          variant="outlined"
          fullWidth
          required
          margin="normal"
      position= "relative"
          value={categoryName}
          onChange={e => setCategory(e.target.value)}
          required
          />
      </label>
      </ModalContent>
      <div>
      <ModalFooter>
      <Button type="submit" size="medium" variant="contained" color="secondary" flex="1" >
          {'Submit'}
        </Button>
        </ModalFooter>
      </div>
    </form>
    <NotificationContainer/>
    </div>
  );
}

export function CustomExample() {
  return (
    <div>
      <Button
        onClick={async () => {
            await CustomDialog(<CategoryDialogContent />, {
            title: 'Create Category',
            showCloseIcon: true,

          });
        }}
      >
        Category
      </Button>
    </div>
  );
}


export default CustomExample;
