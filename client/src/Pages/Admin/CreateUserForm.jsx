import React, { useEffect, useState } from 'react';

import PropTypes from 'prop-types';
import useForm from 'react-hook-form';
import { Link as RouterLink } from 'react-router-dom';
import { Box, TextField, Button, MenuItem } from '@material-ui/core';
import Api from '../../api/config';
import { useStyles } from './styles';
import { history } from '../../_utils/history';
import DateFnsUtils from '@date-io/date-fns';
import { addDays } from 'date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import { fieldbirthday, fields, fieldstagiaire} from './createUserFields';
import { formatDateForFields } from '../../_utils/dateHelpers';

import {NotificationManager} from 'react-notifications';

import { adminRoles } from './adminRoles';
import { superAdminRoles } from './superAdminRoles';

import { typeformateur } from './formateurType';
import { getCurrentUser } from '../../_actions';


//Auto complete
import AutoCompleteCourses from './AutoCompleteCourses';
import CategoriesCoursesAutoComplete from '../CourseCreator/CategoriesCoursesAutoComplete';
import { connect } from 'react-redux';


const CreateUserForm = ({ updateUsers,user,getCurrentUser }) => {
  useEffect(() => {
    getCurrentUser();
 

  }, [getCurrentUser]);
  console.log("ROOOOOOLE form==>"+user.role);
  const roleByCurrentUser = [];
 

  if(user.role=="ROLE_SUPER_ADMIN"){
    for (const [index, value] of superAdminRoles.entries()) {
      roleByCurrentUser.push(value);
    }
  }else{
   // this.roleByCurrentUser=adminRoles
   for (const [index, value] of adminRoles.entries()) {
    roleByCurrentUser.push(value);
  }
  }

  const classes = useStyles();
  const [selectedRole,setSelectedRole]= React.useState();
  const [birthDate, setBirthDate] = useState(addDays(new Date(), 7));
  const [formation,setFormation]= React.useState();
  const [selectedFormateurType,setSelectedFormateurType]= React.useState();


  ///
  const { register, handleSubmit, getValues, setValue } = useForm({
    mode: 'onBlur',
    defaultValues: {
      role: superAdminRoles[1].key
    },
  }

  );
  ///////////////////////////// User effect //////////////////
  useEffect(() => {
   // getCurrentUser()
   


    register({ name: 'role', type: 'text', required: true });
    register({ name: 'typeFormateur', type: 'text', required: false });
    register({ name: 'birthDate', type: 'text', required: false });
    register({ name: 'formations', type: 'text', required: false });
    register({ name: 'categories', type: 'text', required: false });

  });

////////////////Submit///////////////////////////////////
  const onSubmit = () => {
     Api().post('admin/add_user', getValues())
     .then(response=>{
      updateUsers();
     // NotificationManager.success('Utilisateur ajouté avec succès', 'SUCCESS');
      history.push('/admin/users')
     })
     .catch(error=>{
      NotificationManager.error('Réessayer', 'FAILED');
     })
   // console.log('list',list)
  
  }


  //// HANDLE CHANGE ELEMENT LAYOUT /////////////////////////
  const handleChange = event => {
    console.log("CreateUserForm get User   "+  user )
    setSelectedRole(event.target.value)
    setValue('role', event.target.value);
  }

  const handleChangeFormateurType = typeFormateur => {
    setSelectedFormateurType(typeFormateur.target.value)
    setValue('typeFormateur', typeFormateur.target.value);
  }

  const handleDateChange = date => {
    setBirthDate(formatDateForFields(date));
    setValue('birthDate', formatDateForFields(date));
  }
  // const handleCoursesChange = formation => {

const handleCoursesChange = ({ target }, fieldName) => {
    console.log('Value ',  "test"+ fieldName)
    setValue('formations',fieldName)
    setFormation(fieldName)
}

const handleCategorieChange = ({ target }, fieldName) => {
  console.log('Value categorie ',  "test"+ fieldName)
  setValue('categories',fieldName)
}


  return (
    <div>
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1>user</h1>
    <TextField
        label="Select user role"
        id="role"
        name="role"
        variant="outlined"
        select
        fullWidth
        required
        margin="normal"
       value={selectedRole}
        onChange={handleChange}
      >
        
        {roleByCurrentUser.map(role => (
          <MenuItem  key={role.key} value={role.key}>
            {role.name}
          </MenuItem>
        ))}
      </TextField>

      {fields.map(field => (
     <TextField
          key={field.id}
          {...field}
          variant="outlined"
          fullWidth
          required
          margin="normal"
          inputRef={register({
            required: true,
          })}
        />
      ))}
     

      {selectedRole==="ROLE_LISTENER" && <TextField
          id='company'
          key={fieldstagiaire.id}
          {...fieldstagiaire}
          variant="outlined"
          fullWidth
          required
          margin="normal"
          inputRef={register({
            required: false,
          })}
        />
      }

      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
        id='birthDate'
        key={fieldbirthday.id}
          {...fieldbirthday}
          value={birthDate}
          onChange={handleDateChange}
          disablePast
          disableToolbar
          fullWidth
          variant="inline"
          margin="normal"
          KeyboardButtonProps={{
            'aria-label': 'Birthday',
          }}
          inputRef={register({
            required: true,
          })}
        />
             
      </MuiPickersUtilsProvider>

       {selectedRole==="ROLE_LISTENER" && <AutoCompleteCourses
          stateChanger={handleCoursesChange}
         
        ></AutoCompleteCourses>
      }  
         {selectedRole==="ROLE_LECTURER" && <CategoriesCoursesAutoComplete
          categoryChanger={handleCategorieChange}
         
        ></CategoriesCoursesAutoComplete>
      }

      {selectedRole==="ROLE_LECTURER" && <TextField
        label="Select Formateur type"
        id="typeFormateur"
        name="typeFormateur"
        variant="outlined"
        select
        fullWidth
        required
        margin="normal"
        value={selectedFormateurType}
        inputRef={register({
          required: false,
        })}
        onChange={handleChangeFormateurType}
      >
        {typeformateur.map(typeformateur => (
          <MenuItem key={typeformateur.key} value={typeformateur.key}>
            {typeformateur.name}
          </MenuItem>
        ))}
      </TextField>
      }

      <Box display="flex" alignItems="stretch" className={classes.formButtonGroup}>
        <Button type="submit" size="large" variant="contained" color="primary" fullWidth>
          {'Add'}
        </Button>
        <Button type="reset" size="large" to={'/admin/users'} component={RouterLink} fullWidth>
          {'Cancel'}
        </Button>
      </Box>
    </form>
    </div>
  );
};


CreateUserForm.propTypes = {
  updateUsers: PropTypes.func.isRequired,
  getCurrentUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  user: state.userReducer.currentUser,
});

const mapDispatchToProps = {
  getCurrentUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateUserForm);

