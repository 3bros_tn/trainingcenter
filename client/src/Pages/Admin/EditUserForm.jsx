import React, { useEffect,useState } from 'react';
import useForm from 'react-hook-form';
import { Link as RouterLink } from 'react-router-dom';
import { fieldbirthday, fields, fieldstagiaire} from './editUserFields';
import { Box, TextField, Button, MenuItem, LinearProgress } from '@material-ui/core';
import DateFnsUtils from '@date-io/date-fns';
import { addDays } from 'date-fns';
import { formatDateForFields } from '../../_utils/dateHelpers';
import {NotificationManager} from 'react-notifications';
import PropTypes from 'prop-types';
import Api from '../../api/config';
import { history } from '../../_utils/history';
import { adminRoles } from './adminRoles';
import { useStyles } from './styles';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { connect } from 'react-redux';
const EditUserForm = ({ userId, userData, updateUsers }) => {
const classes = useStyles();
const handleDateChange = date => {
    setBirthDate(formatDateForFields(date));
    setValue('birthDate', formatDateForFields(date));
  }

  const [birthDate, setBirthDate] = useState(addDays(new Date(), 7));
  const [role,setRole] = React.useState('');
  const [info,setInfo] = React.useState('');
  const { handleSubmit, register, getValues, setValue } = useForm({
    mode: 'onBlur',
  });

  const handleChange = event => {
    setValue('role', event.target.value);
    setRole(event.target.value)
  };
  const textChange = event => {
   
   // this.setState("fff")

    setInfo(event.target.value)

  }
  const onSubmit = () => {
    console.log("values",getValues())
  /*  Api().put(`admin/${userId}/update_user`, getValues())
    .then(response=>{
     updateUsers();
    // NotificationManager.success('Utilisateur ajouté avec succès', 'SUCCESS');
     history.push('/admin/users')
    })
    .catch(error=>{
     NotificationManager.error('Réessayer', 'FAILED');
    })*/
    Api().put(`admin/${userId}/update_user`, getValues());
    //updateUsers();
    history.push('/admin/users');
  };

 useEffect(() => {
    
    register({ name: 'role', type: 'text', required: true });
    register({ name: 'birthDate', type: 'text', required: false });
    setValue('role', userData.role);
    setValue('birthDate',userData.birthDate);

   // console.log("user data ",userData)
   setRole(userData.role)
   setInfo(userData)
   setBirthDate(new Date((userData.birthDate)))
   console.log("values",getValues())

  }, [register, userData.role]); 
  return (
    //userData.firstName.length ? (
      <form onSubmit={handleSubmit(onSubmit)}>
      
        {fields.map(field => (
          <TextField
            key={field.name}
            {...field}
            variant="outlined"
            value={info[field.name]}
            fullWidth
            required
            margin="normal"
            InputLabelProps={{ shrink: true }}
            inputRef={register({
              required: 'Required',
            })}
            onChange={textChange} 
          />
        ))}
        <TextField
          label="Select user role"
          id="role"
          name="role"
          variant="outlined"
          value={role}
          select
          fullWidth
          required
          margin="normal"
          onChange={handleChange}
        >
          {adminRoles.map(role => (
            <MenuItem key={role.key} value={role.key}>
              {role.name}
            </MenuItem>
          ))}
        </TextField>
  
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
        id='birthDate'
        key={fieldbirthday.id}
          {...fieldbirthday}
          value={birthDate}
          onChange={handleDateChange}
          disablePast
          disableToolbar
          fullWidth
          variant="inline"
          margin="normal"
          KeyboardButtonProps={{
            'aria-label': 'Birthday',
          }}
          inputRef={register({
            required: true,
          })}
        />
             
      </MuiPickersUtilsProvider>
        <Box display="flex" alignItems="stretch" className={classes.formButtonGroup}>
          <Button type="submit" size="large" variant="contained" color="primary" fullWidth>
            {'Edit'}
          </Button>
          <Button size="large" to={'/admin/users'} component={RouterLink} fullWidth>
            {'Cancel'}
          </Button>
        </Box>
      </form>
 //   ) : <LinearProgress/>
  );
  
};

EditUserForm.propTypes = {
  updateUsers: PropTypes.func.isRequired,
};



export default connect(

)(EditUserForm);
