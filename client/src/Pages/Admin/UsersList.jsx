import React from 'react';
import { Link as RouterLink, useRouteMatch } from 'react-router-dom';
import { deleteUser } from '../../_actions';
import { connect } from 'react-redux';
import {
  IconButton,
  Grid,
  Paper,
  Box,
  Avatar,
  Typography,
  Link,
  Button,
  Divider,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  ListItemSecondaryAction,
} from '@material-ui/core';
import { DeleteRounded, EditRounded as EditRoundedIcon } from '@material-ui/icons';

const UsersList = ({ users, currentUserId, deleteUser }) => {
  const { path } = useRouteMatch();

  return (
    <Grid item xs={12}>
      <Box py={1} px={2} my={1} display="flex" alignItems="baseline" justifyContent="space-between">
        <Typography variant="h4" component="h2">
          Users
        </Typography>
        <Button component={RouterLink} to={`${path}/user/create`}>
          {'Add new'}
        </Button>
      </Box>
      <Paper>
        <Box p={1} mb={1}>
          <List>
            {users.map(({ id, firstName, lastName, email, role }) => {
              return (
                <React.Fragment key={`userItem ${id}`}>
                  <ListItem alignItems="flex-start">
                    <ListItemAvatar>
                      <Avatar alt={firstName} src={`https://i.pravatar.cc/150?u=${id}`}></Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={firstName + ' ' + lastName}
                      secondary={
                        <Link color="secondary" href={`mailto:${email}`}>
                          {email}
                        </Link>
                      }
                    />
                    <ListItemText display="flex-container" text-align="left" >
                      {
                        {
                          ROLE_SUPER_ADMIN: <h6>super Admin</h6>,
                          ROLE_ADMIN: <h6>Admin</h6>,
                          ROLE_LECTURER: <h6>Formateur</h6>,
                          ROLE_LISTENER: <h6>Stagiaire</h6>,
                        }[role]
                      }
                    </ListItemText>
                    {currentUserId !== id ? (
                      <ListItemSecondaryAction>
                        <IconButton to={`${path}/edit/${id}`} component={RouterLink}>
                          <EditRoundedIcon />
                        </IconButton>
                        <IconButton onClick={()=>deleteUser(email)}>
                          <DeleteRounded />
                        </IconButton>
                      </ListItemSecondaryAction>
                    ) : (
                      ''
                    )}
                  </ListItem>
                  <Divider variant="inset" component="li" />
                </React.Fragment>
              );
            })}
          </List>
        </Box>
      </Paper>
    </Grid>
  );
};

const mapStateToProps = state => ({
  currentUserId: state.userReducer.currentUserId,
});

const mapDispatchToProps = {
  deleteUser,
};

export default connect(mapStateToProps, mapDispatchToProps) (UsersList);
