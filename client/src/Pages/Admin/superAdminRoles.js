export const superAdminRoles = [{
    key: 'ROLE_ADMIN',
    name: 'Admnistrateur'
  },
  {
    key: 'ROLE_LECTURER',
    name: 'Formateur'
  },
  {
    key: 'ROLE_LISTENER',
    name: 'Stagiaire'
  }
];
