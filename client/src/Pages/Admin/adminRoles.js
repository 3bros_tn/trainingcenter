export const adminRoles = [
  {
    key: 'ROLE_LECTURER',
    name: 'Formateur',
  },
  {
    key: 'ROLE_LISTENER',
    name: 'Stagiaire',
  },
  {
    key: 'ROLE_SUPER_ADMIN',
    name: 'Super Admin',
  }

];
