import React from 'react';
import { useEffect,useState } from 'react';
import { connect } from 'react-redux';
import { Grid, Paper, Box, Typography } from '@material-ui/core';
import { getUserById } from '../../_actions';
import PropTypes from 'prop-types';
import Api from '../../api/config';

import EditUserForm from './EditUserForm';
const divStyle = {
  marginLeft: '23%',
  
}; 
class EditUser extends React.Component {
  constructor(props) {
     super(props);
    this.state = {
      userId: window.location.href.split('/')[6],
    };

    console.log("const ",this.props.element)
  }

  componentDidMount() {
    
    this.props.getUserById(this.state.userId);
    console.log(this.state.userId+" componentDidMount ",this.props);

  }

  render() {
   /* this.setState((state) => {
      this.props.getUserById(this.state.userId);
    });*/
    return (
      <Grid item sm={7} style={divStyle}>
        <Paper>
          <Box p={2}>
            <Typography variant="h5" component="h2">
              {'Edit user'}
            </Typography>
           
          </Box>
        </Paper>
      </Grid>
    );
  }
}

const App = (user) => {
  const [currentUser,setCurrentUser]=useState([])
  useEffect(() => {
    fetchCurrentUser();
  }, [])
  useEffect(() => {
  }, [currentUser])
  const fetchCurrentUser=async()=>{
    Api().get('user_profile/'+window.location.href.split('/')[6])
    .then(response=>{
     setCurrentUser(response.data)    
    });
   
  }
  return (
    <Grid item sm={7} style={divStyle}>
        <Paper>
          <Box p={2}>
            <Typography variant="h5" component="h2">
              {'Edit user'}
            </Typography>
            <EditUserForm
              userId={currentUser.id}
              userData={currentUser}
            //  updateUsers={this.props.updateUsers}
            />
          </Box>
        </Paper>
      </Grid>


  );
};
App.propTypes = {
  getUserById: PropTypes.func.isRequired,
};
const mapStateToProps = state => ({
  user: state.userReducer.userData,
  
});

const mapDispatchToProps = {
  getUserById,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
