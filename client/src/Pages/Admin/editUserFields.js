/*export const fields = [
  {
    id: 'firstName',
    name: 'firstName',
    label: 'First Name',
  },
  {
    id: 'lastName',
    name: 'lastName',
    label: 'Last Name',
  },
  {
    id: 'email',
    name: 'email',
    label: 'Email',
    type: 'email',
  },
  {
    id: 'skype',
    name: 'skype',
    label: 'Skype',
  },
];
*/
export const fields = [
  {
    id: 'firstName',
    name: 'firstName',
    label: 'First Name',
  },
  {
    id: 'lastName',
    name: 'lastName',
    label: 'Last Name',
  },
  {
    id: 'password',
    name: 'password',
    label: 'Password',
    type: 'password',
  },
  {
    id: 'email',
    name: 'email',
    label: 'Email',
    type: 'email',
  },
  {
    id: 'phoneNumber',
    name: 'phoneNumber',
    label: 'Phone Number',
  }
];

export const fieldbirthday = {
  id: 'birthDate',
  label: 'Birth Date',
  name: 'birthDate',
};

export const fieldstagiaire = {
  id: 'company',
  label: 'company',
  name: 'company',
  type: 'text',
};
export const fieldformation = {
  id: 'formation',
  label: 'formation',
  name: 'formation',
};