import { getAllCourses } from '../../_actions/courses.actions';
import Autocomplete from '@mui/material/Autocomplete';
import { TextField } from '@material-ui/core';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {fieldformation} from './createUserFields';
import {NotificationContainer, NotificationManager} from 'react-notifications';

const AutoCompleteCourses = ({ getAllCourses, courses, coursesLoading ,stateChanger}) => {
    useEffect(() => {
      getAllCourses();
    }, [getAllCourses]);

    const formations = []
    for (const [index, value] of courses.entries()) {
     // console.log("value.lecturers"+JSON.parse(value.lecturers))
      
        console.log("********")
        if(value.lecturers.length === 0){
        formations.push(value.courseName)
        }else {
            //console.log(value.lecturers.entries())
             for (const [index2, value2] of value.lecturers) {
                formations.push(value.courseName + "("+value2.firstName+")")
                 }
            }
        }
      
        

    return (
    
    <Autocomplete
        key={fieldformation.id}
        {...fieldformation}
        disablePortal
        id='formations'
        name="formations"
        options={formations}
        multiple
        //onSelect={(event) => stateChanger(event)}
        onChange={ stateChanger}

        getOptionLabel={(option)=>option}
        renderInput={(params)=>
            <TextField {...params}
            variant="outlined"
            fullWidth
            margin="normal"
            label="Formations"
            ></TextField>
        }
         ></Autocomplete>
         
         
      );


  }
  
  AutoCompleteCourses.propTypes = {
    courses: PropTypes.array.isRequired,
    getAllCourses: PropTypes.func.isRequired,
  };
  const mapStateToProps = state => ({
    courses: state.coursesReducer.courses,
    coursesLoading: state.coursesReducer.isRequestProcessing,
  });
  const mapDispatchToProps = {
    getAllCourses,
  };
  export default connect(
    mapStateToProps,
    mapDispatchToProps,
  )(AutoCompleteCourses);