import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  formButtonGroup: {
    margin: theme.spacing(2, -1, 0, -1),
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  buttonLayout:{
    container: {
      flex: 1,
      justifyContent: 'center',
    },
    headerText: {
      fontSize: 20,
      textAlign: "center",
      margin: 10,
      fontWeight: "bold"
    },
    buttonOuterLayout: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    buttonLayout: {
      marginBottom: 10
    } 
  }
}));

