package by.mikhalkovich.training_center_v2.dto;

import java.time.LocalDate;
import java.util.List;

import by.mikhalkovich.training_center_v2.model.Course;
import by.mikhalkovich.training_center_v2.model.Lecturer;
import by.mikhalkovich.training_center_v2.model.Listener;
import lombok.Data;

@Data
public class CourseDto {

    private Long id;
    private String courseName;
    private String courseDescription;
    private int courseDuration;
    private LocalDate startDate;
    private LocalDate endDate;
    private String category;
    private List<Lecturer> lecturers;
    private List<Listener> listeners;

    public static CourseDto fromCourse(Course course){
        CourseDto courseDto = new CourseDto();
        courseDto.setId(course.getId());
        courseDto.setCourseName(course.getCourseName());
        courseDto.setCourseDescription(course.getCourseDescription());
        courseDto.setCourseDuration(course.getCourseDuration());
        courseDto.setStartDate(course.getStartDate());
        courseDto.setCategory(course.getCategory());
        courseDto.setListeners(course.getListener());
        courseDto.setEndDate(course.getEndDate());
        return courseDto;
    }
    
    public static CourseDto fromCourse(Course course, List<Lecturer> lecturers){
        CourseDto courseDto = new CourseDto();
        courseDto.setId(course.getId());
        courseDto.setCourseName(course.getCourseName());
        courseDto.setCourseDescription(course.getCourseDescription());
        courseDto.setCourseDuration(course.getCourseDuration());
        courseDto.setStartDate(course.getStartDate());
        courseDto.setEndDate(course.getEndDate());
        courseDto.setCategory(course.getCategory());
        courseDto.setListeners(course.getListener());
        courseDto.setLecturers(lecturers);
        return courseDto;
    }
}
