package by.mikhalkovich.training_center_v2.dto;

import by.mikhalkovich.training_center_v2.model.Role;
import by.mikhalkovich.training_center_v2.model.User;
import lombok.Data;

@Data
public class UserProfile {
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String categories;

    private Role role;
    private String password;
    private String birthDate;
    public static UserProfile fromUser(User user){
        UserProfile userProfile = new UserProfile();
        userProfile.setId(user.getId());
        userProfile.setEmail(user.getUsername());
        userProfile.setFirstName(user.getFirstName());
        userProfile.setLastName(user.getLastName());
        userProfile.setPhoneNumber(user.getPhoneNumber());
        userProfile.setRole(user.getRole());
        userProfile.setCategories(user.getCategories());
        userProfile.setPassword(user.getPassword());
        userProfile.setBirthDate(user.getBirthDate());

        return userProfile;
    }
}
