package by.mikhalkovich.training_center_v2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

import java.util.Date;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "email")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "company")
	private String company;

	@Column(name = "type_formateur")
	private String typeFormateur;

	@Column(name = "categories")
	private String categories;

	@Column(name = "birth_date")
	private String birthDate;

	@Column(name = "role")
	@Enumerated(EnumType.STRING)
	private Role role;

	@Column(name = "creation_date")
	private Date created;

	@Column(name = "update_date")
	private Date updated;

	@PrePersist
	protected void onCreate() {
		created = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		updated = new Date();
	}

	public User() {
	}

	@OneToOne(mappedBy = "user")
	@JsonIgnore
	private Listener listener;

	@OneToOne(mappedBy = "user")
	private Lecturer lecturer;

	@Override
	public boolean equals(Object otherObject) {
		if (otherObject == this)
			return true;
		if (otherObject == null)
			return false;
		if (!(otherObject instanceof User))
			return false;

		User other = (User) otherObject;

		return this.id.equals(other.id);
	}

	@Override
	public int hashCode() {
		return 31 * Objects.hashCode(id);
	}

}
