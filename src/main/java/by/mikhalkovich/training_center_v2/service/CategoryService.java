package by.mikhalkovich.training_center_v2.service;

import java.util.List;

import by.mikhalkovich.training_center_v2.model.Categories;

public interface CategoryService {
	
	void addCategory(Categories category);
	List<Categories> getAllCategories();
}
