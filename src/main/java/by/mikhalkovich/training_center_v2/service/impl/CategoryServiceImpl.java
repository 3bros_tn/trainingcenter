package by.mikhalkovich.training_center_v2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.mikhalkovich.training_center_v2.model.Categories;
import by.mikhalkovich.training_center_v2.repository.CategoryRepository;
import by.mikhalkovich.training_center_v2.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService{

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public void addCategory(Categories category) {
		this.categoryRepository.save(category);
	}

	@Override
	public List<Categories> getAllCategories() {
		return this.categoryRepository.findAll();
	}

}
