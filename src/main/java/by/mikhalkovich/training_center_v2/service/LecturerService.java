package by.mikhalkovich.training_center_v2.service;

import java.util.List;

import by.mikhalkovich.training_center_v2.model.Lecturer;

public interface LecturerService {

    Lecturer save(Lecturer lecturer);
    Lecturer findByUserId(Long id);
	List<Lecturer> findLecturerByCategory(String category);
}
