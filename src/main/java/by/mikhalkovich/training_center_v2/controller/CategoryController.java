package by.mikhalkovich.training_center_v2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import by.mikhalkovich.training_center_v2.model.Categories;
import by.mikhalkovich.training_center_v2.service.CategoryService;

@RestController("/category")
@CrossOrigin
public class CategoryController {

	private CategoryService categoryService;
	
	@Autowired
	public CategoryController(CategoryService categoryService) {
		super();
		this.categoryService = categoryService;
	}

	@GetMapping("/category/all")
	public List<Categories> getAllCategories() {
		 return categoryService.getAllCategories();
	}
	
	@PostMapping("/category/add_category")
	public String saveCategory(@RequestBody Categories category) {
		this.categoryService.addCategory(category);
		return "category added successfully";
	}
}
