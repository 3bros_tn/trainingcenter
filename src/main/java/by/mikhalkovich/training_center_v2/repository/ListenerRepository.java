package by.mikhalkovich.training_center_v2.repository;

import by.mikhalkovich.training_center_v2.model.Listener;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface ListenerRepository extends JpaRepository<Listener, Long> {

    Listener findByUserId(Long id);
    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "delete from listener u where u.user_id = :id")
	void deleteByIdUser(Long id);
}
