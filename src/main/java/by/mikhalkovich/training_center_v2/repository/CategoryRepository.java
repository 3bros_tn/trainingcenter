package by.mikhalkovich.training_center_v2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import by.mikhalkovich.training_center_v2.model.Categories;

@Repository
public interface CategoryRepository extends JpaRepository<Categories, Long> {

}
