package by.mikhalkovich.training_center_v2.repository;

import by.mikhalkovich.training_center_v2.model.Lecturer;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface LecturerRepository extends JpaRepository<Lecturer, Long> {

    Lecturer findByUserId(Long id);

    @Query(nativeQuery = true,  value = "select l.* from lecturer l inner join category c on c.id = l.category=id where c.category_name= :category")
	List<Lecturer> findByCategory(String category);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "delete from lecturer u where u.user_id = :id")
	void deleteByIdUser(Long id);
}
